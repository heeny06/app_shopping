class Goods {
  String photoUrl;
  String goodsName;
  int goodsPrice;
  String goodsPerform;
  String goodsSpecification;

  Goods(this.photoUrl, this.goodsName, this.goodsPrice, this.goodsPerform, this.goodsSpecification);
  
}