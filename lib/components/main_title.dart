import 'package:flutter/material.dart';

class MainTitle extends StatelessWidget {
  final String title; // final 이니까 required로 받음 필수

  const MainTitle({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
              title,
            style: const TextStyle(
              letterSpacing: 1.5,
              fontSize: 14.0,
              fontWeight: FontWeight.w600
            ),
          ),
        ],
      ),
    );
  }
}
