import 'package:flutter/material.dart';

class GoodsItem extends StatelessWidget {
  GoodsItem({super.key, required this.photoUrl, required this.goodsName, required this.goodsPrice, this.imgWidth = 100, this.imgHeight = 100, required this.callback});

  final String photoUrl;
  final String goodsName;
  final int goodsPrice;
  double imgWidth;
  double imgHeight;
  final VoidCallback callback;


    @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Column(
          children: [
            Image.asset(photoUrl, width: imgWidth, height: imgHeight,),
            Container(
              child: Text(goodsName),
            ),
            Container(
              child: Text(goodsPrice.toString()),
            ),
          ],
        ),
      ),
      onTap: callback,
    );
  }
}
