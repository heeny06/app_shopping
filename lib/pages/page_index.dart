import 'package:app_shopping/components/goods_item.dart';
import 'package:app_shopping/components/main_title.dart';
import 'package:app_shopping/pages/page_goods_detail.dart';
import 'package:flutter/material.dart';

import '../config/config_goods_data.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Image.asset('assets/13.jpg'),
              const SizedBox(
                height: 50,
              ),
              const MainTitle(
                title: 'New',
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 0; i < configGoodsMookup.length; i++)
                      GoodsItem(
                        photoUrl: configGoodsMookup[i].photoUrl,
                        goodsName: configGoodsMookup[i].goodsName,
                        goodsPrice: configGoodsMookup[i].goodsPrice,
                        imgWidth: 150,
                        imgHeight: 150,
                        callback: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PageGoodsDetail(goods: configGoodsMookup[i],)));
                        },
                      ),
                  ],
                ),
              ),
              const SizedBox(
                height: 70,
              ),
              const MainTitle(title: 'Best'),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (int i = 0; i < configGoodsSpecialList.length; i++)
                      GoodsItem(
                          photoUrl: configGoodsSpecialList[i].photoUrl,
                          goodsName: configGoodsSpecialList[i].goodsName,
                          goodsPrice: configGoodsSpecialList[i].goodsPrice,
                          callback: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PageGoodsDetail(goods: configGoodsMookup[i],)));
                          }),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
