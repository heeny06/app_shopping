import 'package:app_shopping/components/goods_info_box.dart';
import 'package:app_shopping/model/goods.dart';
import 'package:flutter/material.dart';

class PageGoodsDetail extends StatefulWidget {
  const PageGoodsDetail({super.key, required this.goods});

  final Goods goods;


  @override
  State<PageGoodsDetail> createState() => _PageGoodsDetailState();
}

class _PageGoodsDetailState extends State<PageGoodsDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Detail'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(widget.goods.photoUrl),
            Container(
              child: Column(
                children: [
                  Text(widget.goods.goodsName),
                  Text(widget.goods.goodsPrice.toString()),
                  Divider(),
                  GoodsInfoBox(title: '정보', contents: widget.goods.goodsPerform),
                  GoodsInfoBox(title: '소재', contents: widget.goods.goodsSpecification),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: OutlinedButton(
          child: const Text('구매하기'),
          onPressed: () {},
        ),
      ),
    );
  }
}
