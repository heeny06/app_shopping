import 'package:app_shopping/model/goods.dart';

final List<Goods> configGoodsMookup = [
  Goods('assets/1.jpg', '예쁜코트', 2000000, '예쁜 코트에요', '캐시미어'),
  Goods('assets/2.jpg', '고급코트', 4000000, '고급 코트에요', '캐시미어'),
  Goods('assets/3.jpg', '가벼운코트', 2500000, '가벼운 코트에요', '캐시미어'),
  Goods('assets/4.jpg', '신상코트', 3700000, '신상 코트에요', '캐시미어'),
  Goods('assets/5.jpg', '유행코트', 2750000, '유행하는 코트에요', '캐시미어'),
  Goods('assets/6.jpg', '분위기있는코트', 2990000, '분위기 쩌는 코트에요', '캐시미어'),
  Goods('assets/7.jpg', '럭셔리한코트', 4900000, '럭셔리한 코트에요', '캐시미어'),
  Goods('assets/8.jpg', '핫한코트', 1800000, '요즘 핫한 코트에요', '캐시미어'),
  Goods('assets/9.jpg', '좋은코트', 3100000, '아주 좋은 코트에요', '캐시미어'),
  Goods('assets/10.jpg', '경조사용코트', 2900000, '경조사용 코트에요', '캐시미어'),
];

final List<Goods> configGoodsSpecialList = [
  Goods('assets/1.jpg', '나들이용코트', 2200000, '나들이용 코트에요', '캐시미어'),
  Goods('assets/2.jpg', '데이트용코트', 3100000, '데이트룩 추천 코트에요', '캐시미어'),
  Goods('assets/3.jpg', '모임용코트', 3600000, '모임룩으로 딱인 코트에요', '캐시미어'),
  Goods('assets/4.jpg', '결혼식용코트', 2900000, '결혼식 갈때 추천하는 코트에요', '캐시미어'),
  Goods('assets/5.jpg', '겨울코트', 3300000, '겨울용 코트에요', '캐시미어'),
  Goods('assets/6.jpg', '초겨울코트', 2890000, '초겨울에 딱인 코트에요', '캐시미어'),
  Goods('assets/7.jpg', '부내나는코트', 5700000, '부내 킁킁 코트에요', '캐시미어'),
  Goods('assets/8.jpg', '강남스타일코트', 5100000, '강남스타일 코트에요', '캐시미어'),
  Goods('assets/9.jpg', '파리지앵코트', 3800000, '파리지앵 코트에요', '캐시미어'),
  Goods('assets/10.jpg', 'mz세대코트', 2700000, 'mz세대 코트에요', '캐시미어'),
];

